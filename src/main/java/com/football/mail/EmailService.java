package com.football.mail;

import com.football.domain.Match;
import com.football.domain.Round;
import com.football.domain.Team;
import com.football.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private TemplateEngine templateEngine;

    public void sendSimpleMessage(String to, String subject, String text) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("javaproiectechipe@gmail.com");
        message.setTo("raresalupoaicei@gmail.com");
        message.setSubject("test1");
        message.setText(text);
        emailSender.send(message);

    }


    public MimeMessagePreparator constructRoundNotificationEmail(User user, Round round, List<Match> matchList, String leagueName, List<Team> leagueTable) {

        Context context = new Context();
        context.setVariable("user", user);
        context.setVariable("round", round);
        context.setVariable("matchList", matchList);
        context.setVariable("leagueName", leagueName);
        context.setVariable("leagueTable", leagueTable);
        String text = templateEngine.process("mailTemplate", context);



        MimeMessagePreparator messagePreparator = new MimeMessagePreparator() {
            @Override
            public void prepare (MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper email = new MimeMessageHelper(mimeMessage);
                email.setTo(user.getEmail());
                email.setSubject("Round Results - " + leagueName);
                email.setText(text, true);
                email.setFrom(new InternetAddress("javaproiectechipe@gmail.com"));
            }
        };
        return messagePreparator;
    }

}