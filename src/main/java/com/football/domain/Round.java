package com.football.domain;


import javax.persistence.*;
import java.util.List;

@Entity
public class Round {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int RoundNumber = 0;

    @OneToMany(mappedBy = "round")
    private List<Match> matches;

    private int year;

    /*@ManyToOne
    @JoinColumn(name = "league_id")
    private League league;*/

    @ManyToOne
    @JoinColumn(name = "tournament_id")
    private Tournament tournament;


    public Round() {
    }

    public Round(int RoundNumber) {
        this.RoundNumber = RoundNumber;
    }

    public Round(int RoundNumber, int year, Tournament tournament) {

        this.RoundNumber = RoundNumber;
        this.year = year;
        this.tournament = tournament;
    }

    public int getRoundNumber() {
        return RoundNumber;
    }

    public void setRoundNumber(int roundNumber) {
        RoundNumber = roundNumber;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public int getId() {
        return id;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    /* public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }*/

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
