package com.football.domain;


import javax.persistence.*;
import java.util.HashMap;
import java.util.List;

@Entity
public class Tournament {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Transient
    String[][] table;

    int nrOfRounds;

    int nrOfTeams;

    int year;

    @ManyToOne
    @JoinColumn(name = "league_id")
    private League league;

    int lastRoundPlayed = 0;

    Boolean ongoing = true;

    @OneToMany(mappedBy = "tournament")
    private List<Round> roundList;

    public Tournament() {
    }

    public Tournament(String[][] table, int nrOfTeams, int nrOfRounds, int year, League league, List<Round> roundList) {
        this.table = table;
        this.nrOfTeams = nrOfTeams;
        this.nrOfRounds = nrOfRounds;
        this.year = year;
        this.league = league;
        this.roundList = roundList;
    }

    public Tournament(int nrOfTeams, int nrOfRounds, int year, League league) {
        this.nrOfTeams = nrOfTeams;
        this.nrOfRounds = nrOfRounds;
        this.year = year;
        this.league = league;
    }

    public String[][] getTable() {
        return table;
    }

    public void setTable(String[][] table) {
        this.table = table;
    }

    public int getNrOfTeams() {
        return nrOfTeams;
    }

    public void setNrOfTeams(int nrOfTeams) {
        this.nrOfTeams = nrOfTeams;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getNrOfRounds() {
        return nrOfRounds;
    }

    public void setNrOfRounds(int nrOfRounds) {
        this.nrOfRounds = nrOfRounds;
    }

    public League getLeague() {
        return league;
    }

    public void setLeague(League league) {
        this.league = league;
    }

    public Boolean getOngoing(){
        return ongoing;
    }

    public void setOngoing(Boolean ongoing){
        this.ongoing = ongoing;
    }

    public List<Round> getRoundList() {
        return roundList;
    }

    public void setRoundList(List<Round> roundList) {
        this.roundList = roundList;
    }

    public int getLastRoundPlayed() {
        return lastRoundPlayed;
    }

    public void setLastRoundPlayed(int lastRoundPlayed) {
        this.lastRoundPlayed = lastRoundPlayed;
    }

    public int getId() {
        return id;
    }
}
