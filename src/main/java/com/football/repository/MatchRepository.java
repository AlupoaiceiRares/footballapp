package com.football.repository;

import com.football.domain.Match;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MatchRepository extends CrudRepository<Match, Integer> {

    List<Match> findByTeam1Equals(String team1);

    List<Match> findByTeam2Equals(String team2);

    Match findById (int id);

    @Query(value = "SELECT m from Match m WHERE m.team1 = ?1 AND m.team2 = ?2")
    List<Match> findByBothTeams(String team1, String team2);
}
