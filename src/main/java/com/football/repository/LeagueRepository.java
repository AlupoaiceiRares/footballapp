package com.football.repository;

import com.football.domain.League;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeagueRepository extends CrudRepository<League, Integer> {


    List<League> findByNameLike(String name);

    @Query(value = "SELECT lg from League lg WHERE lg.name = ?1")
    League findByName(String name);
}
