package com.football.repository;

import com.football.domain.FileUpload;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import java.util.List;

@Repository
public interface FileUploadRepository extends JpaRepository<FileUpload, String> {

    List<FileUpload> findByFilenameLikeAndYearOfTheFileLike(String name, Integer year);

    List<FileUpload> findByMonthOfTheFileLikeAndYearOfTheFileLike(Integer month, Integer year);

    List<FileUpload> findByYearOfTheFile(Integer year);

    @Query(value = "SELECT f from FileUpload f WHERE f.filename = ?1")
    FileUpload findByFilename(String filename);

}
