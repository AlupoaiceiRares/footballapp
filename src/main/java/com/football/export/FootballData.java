package com.football.export;



public class FootballData {


    private int roundID;

    private String leagueName;

    private String nameTeam1;

    private String nameTeam2;

    private String score;

    private String formationTeam1;

    private String formationTeam2;

    /*private float ratingTeam1GK;

    private float ratingTeam2GK;

    private float ratingTeam1DEF;

    private float ratingTeam2DEF;

    private float ratingTeam1MID;

    private float ratingTeam2MID;

    private float ratingTeam1ATT;

    private float ratingTeam2ATT;*/

    public FootballData() {
    }

    public FootballData(FootballData footballData){
        this.roundID = footballData.getRoundID();
        this.leagueName = footballData.getLeagueName();
        this.nameTeam1 = footballData.getNameTeam1();
        this.nameTeam2 = footballData.getNameTeam2();
        this.score = footballData.getScore();
        this.formationTeam1 = footballData.getFormationTeam1();
        this.formationTeam2 = footballData.getFormationTeam2();
       /* this.ratingTeam1GK = footballData.getRatingTeam1GK();
        this.ratingTeam2GK = footballData.getRatingTeam2GK();
        this.ratingTeam1DEF = footballData.getRatingTeam1DEF();
        this.ratingTeam2DEF = footballData.getRatingTeam2DEF();
        this.ratingTeam1MID = footballData.getRatingTeam1MID();
        this.ratingTeam2MID = footballData.getRatingTeam2MID();
        this.ratingTeam1ATT = footballData.getRatingTeam1ATT();
        this.ratingTeam2ATT = footballData.getRatingTeam2ATT();*/
    }

    //, float ratingTeam1GK, float ratingTeam2GK, float ratingTeam1DEF, float ratingTeam2DEF, float ratingTeam1MID, float ratingTeam2MID, float ratingTeam1ATT, float ratingTeam2ATT
    public FootballData(int roundID, String leagueName, String nameTeam1, String nameTeam2, String score, String formationTeam1, String formationTeam2) {
        this.roundID = roundID;
        this.leagueName = leagueName;
        this.nameTeam1 = nameTeam1;
        this.nameTeam2 = nameTeam2;
        this.score = score;
        this.formationTeam1 = formationTeam1;
        this.formationTeam2 = formationTeam2;
        /*this.ratingTeam1GK = ratingTeam1GK;
        this.ratingTeam2GK = ratingTeam2GK;
        this.ratingTeam1DEF = ratingTeam1DEF;
        this.ratingTeam2DEF = ratingTeam2DEF;
        this.ratingTeam1MID = ratingTeam1MID;
        this.ratingTeam2MID = ratingTeam2MID;
        this.ratingTeam1ATT = ratingTeam1ATT;
        this.ratingTeam2ATT = ratingTeam2ATT;*/
    }

    public int getRoundID() {
        return roundID;
    }

    public void setRoundID(int roundID) {
        this.roundID = roundID;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    public String getNameTeam1() {
        return nameTeam1;
    }

    public void setNameTeam1(String nameTeam1) {
        this.nameTeam1 = nameTeam1;
    }

    public String getNameTeam2() {
        return nameTeam2;
    }

    public void setNameTeam2(String nameTeam2) {
        this.nameTeam2 = nameTeam2;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getFormationTeam1() {
        return formationTeam1;
    }

    public void setFormationTeam1(String formationTeam1) {
        this.formationTeam1 = formationTeam1;
    }

    public String getFormationTeam2() {
        return formationTeam2;
    }

    public void setFormationTeam2(String formationTeam2) {
        this.formationTeam2 = formationTeam2;
    }

    /*public float getRatingTeam1GK() {
        return ratingTeam1GK;
    }

    public void setRatingTeam1GK(float ratingTeam1GK) {
        this.ratingTeam1GK = ratingTeam1GK;
    }

    public float getRatingTeam2GK() {
        return ratingTeam2GK;
    }

    public void setRatingTeam2GK(float ratingTeam2GK) {
        this.ratingTeam2GK = ratingTeam2GK;
    }

    public float getRatingTeam1DEF() {
        return ratingTeam1DEF;
    }

    public void setRatingTeam1DEF(float ratingTeam1DEF) {
        this.ratingTeam1DEF = ratingTeam1DEF;
    }

    public float getRatingTeam2DEF() {
        return ratingTeam2DEF;
    }

    public void setRatingTeam2DEF(float ratingTeam2DEF) {
        this.ratingTeam2DEF = ratingTeam2DEF;
    }

    public float getRatingTeam1MID() {
        return ratingTeam1MID;
    }

    public void setRatingTeam1MID(float ratingTeam1MID) {
        this.ratingTeam1MID = ratingTeam1MID;
    }

    public float getRatingTeam2MID() {
        return ratingTeam2MID;
    }

    public void setRatingTeam2MID(float ratingTeam2MID) {
        this.ratingTeam2MID = ratingTeam2MID;
    }

    public float getRatingTeam1ATT() {
        return ratingTeam1ATT;
    }

    public void setRatingTeam1ATT(float ratingTeam1ATT) {
        this.ratingTeam1ATT = ratingTeam1ATT;
    }

    public float getRatingTeam2ATT() {
        return ratingTeam2ATT;
    }

    public void setRatingTeam2ATT(float ratingTeam2ATT) {
        this.ratingTeam2ATT = ratingTeam2ATT;
    }*/
}
