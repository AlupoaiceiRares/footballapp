package com.football.export;




import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;


import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.apache.poi.ss.util.CellUtil.createCell;

@Slf4j
public class ExcelExporter {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<FootballData> footballDataList;
    private final static int MAX_NUMBER_OF_COLUMNS = 6;

    static byte[] blackColor = {(byte) 0, (byte) 0, (byte) 0};
    public static XSSFColor BLACK_BACKGROUND = new XSSFColor(blackColor, null);
    static byte[] greenColor = {(byte) 9, (byte) 250, (byte) 10};
    public static XSSFColor GREEN_WRITING = new XSSFColor(greenColor, null);

    static byte[] lightgreenColor={(byte)199 ,(byte) 255, (byte) 199};
    public static XSSFColor GREEN_BACKGROUND = new XSSFColor(lightgreenColor, null);



    public ExcelExporter(List<FootballData> footballDataList) {
        this.footballDataList = footballDataList;
        workbook = new XSSFWorkbook();
    }

    private void writeHeaderLine() {


        Row row = sheet.createRow(1);

        XSSFCellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);
        style.setFillForegroundColor(GREEN_BACKGROUND);
        style.setFillBackgroundColor(GREEN_BACKGROUND);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        createCell(row, 0, "", style);
        createCell(row, 1, "Home Team", style);
        createCell(row, 2, "Away Team", style);
        createCell(row, 3, "Score", style);
        createCell(row, 4, "Formation Home Team", style);
        createCell(row, 5, "Formation Away Team", style);

        for (int j = 0; j < MAX_NUMBER_OF_COLUMNS; j++) {
            sheet.autoSizeColumn(j);
        }
    }

    private void writeRoundLine(int rowNr, int roundNr, String name){
        Row row = sheet.createRow(rowNr);
        int col = 0;
        XSSFCellStyle style = roundLineStyle(workbook);
        createCell(row,col,"Round: " + String.valueOf(roundNr),style);
        col = col +1;
        while(col<MAX_NUMBER_OF_COLUMNS){
           createCell(row,col,"",style);
           col++;
        }
    }

    public static XSSFCellStyle roundLineStyle(XSSFWorkbook workbook) {
        XSSFCellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(13);
        font.setColor(GREEN_WRITING);
        style.setFont(font);
        style.setFillForegroundColor(BLACK_BACKGROUND);
        style.setFillBackgroundColor(BLACK_BACKGROUND);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setAlignment(HorizontalAlignment.LEFT);

        return  style;
    }

    private void writeLeagueHeader(String name){
        sheet = workbook.createSheet("LeagueSummary");
        Row row = sheet.createRow(0);
        int col = 0;
        XSSFCellStyle style = roundLineStyle(workbook);
        createCell(row,col, name,style);
        col = col +1;
        /*createCell(row,col,name,style);
        col ++;*/
        while(col<MAX_NUMBER_OF_COLUMNS){
            createCell(row,col,"",style);
            col++;
        }

    }


    private void writeDataLines(){
        log.info("The writing of data has started");
        int rowCount = 2;
        int roundNr = 0 ;

        XSSFCellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);
        style.setFillForegroundColor(GREEN_BACKGROUND);
        style.setFillBackgroundColor(GREEN_BACKGROUND);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setAlignment(HorizontalAlignment.CENTER);

        for (FootballData footballData : footballDataList) {
            if(footballData.getRoundID() == roundNr + 1){
                writeRoundLine(rowCount,footballData.getRoundID(),footballData.getLeagueName());
                roundNr++;
                rowCount++;
            }

            Row row = sheet.createRow(rowCount);

            int column = 0;
            createCell(row,column++,"", style);
            createCell(row, column++, footballData.getNameTeam1(), style);
            createCell(row, column++, footballData.getNameTeam2(), style);
            createCell(row, column++, footballData.getScore(), style);
            createCell(row, column++, footballData.getFormationTeam1(), style);
            createCell(row, column++, footballData.getFormationTeam2(), style);

            rowCount++;
        }
        for (int j = 0; j < MAX_NUMBER_OF_COLUMNS; j++) {
            sheet.autoSizeColumn(j);
        }
        log.info("The writing of data has ended");
    }


    public void export(HttpServletResponse response) throws IOException {
        writeLeagueHeader(footballDataList.get(0).getLeagueName());
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}
