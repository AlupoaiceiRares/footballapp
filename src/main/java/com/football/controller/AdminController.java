package com.football.controller;

import com.football.domain.*;
import com.football.mail.EmailService;
import com.football.service.*;
import com.football.serviceUtils.FootballUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Controller
@Slf4j
public class AdminController {

    @Autowired
    private PlayerService playerService;

    @Autowired
    private LeagueService leagueService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private FootballUtils footballUtils;

    @Autowired
    private RoundService roundService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    private final String UPLOAD_DIR = "./";


    @GetMapping("/addPlayer")
    public String addPlayer(Model model) {
        model.addAttribute("player", new Player());

        return "/playerAddForm";
    }

    @PostMapping("/addPlayer")
    public String registerUser(@Valid Player player, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            return "playerAddForm";
        }
        else{
            playerService.createPlayer(player);
            //return "registerForm";
            return "succes2";
        }
    }


    @GetMapping("/addLeague")
    public String addLeague(Model model) {
        model.addAttribute("league", new League());

        return "/leagueAddForm";
    }


    @PostMapping("/addLeague")
    public String registerUser(@Valid League league, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            return "leagueAddForm";
        }
        else{
            leagueService.CreateLeague(league);
            //return "registerForm";
            return "succes2";
        }
    }

    @GetMapping("/updatePlayer")
    public String updatePlayer(Model model){
        model.addAttribute("playerUpdate", new Player());

        return "/playerUpdateForm";
    }

    @PostMapping("/updatePlayer")
    public String updatePLayer(@Valid Player player, BindingResult bindingResult,
                               Model model, @RequestParam(defaultValue = "") String oldFirstname, @RequestParam(defaultValue = "") String oldLastname, @RequestParam(defaultValue = "") String oldPosition  ){
        if(bindingResult.hasErrors()){
            return "playerUpdateForm";
        } else {
            Player oldPlayer = playerService.findByFirstnameLastnamePosition(oldFirstname,oldLastname,oldPosition);
            if(oldPlayer != null){
                playerService.updatePlayer(oldPlayer,player);
                return "succesUpdate";
            }
        }
        return "failed";
    }

    @GetMapping("/updateTeam")
    public String updateTeam(Model model){
        model.addAttribute("teamUpdate", new Team());

        return "/teamUpdateForm";
    }

    @GetMapping("/updateLeague")
    public String updateLeague(Model model){
        model.addAttribute("leagueUpdate", new League());

        return "/leagueUpdateForm";
    }

    @PostMapping("/updateLeague")
    public String updateLeague(@Valid League league, BindingResult bindingResult, Model model, @RequestParam(defaultValue = "") String oldName ){
        if(bindingResult.hasErrors()){
            return "leagueUpdateForm";
        } else {
            leagueService.UpdateTeamOldToNew(oldName, league.getName());
            return "succesUpdate";
        }
    }

    @PostMapping("/updateTeam")
    public String updateTeam(@Valid Team team, BindingResult bindingResult, Model model, @RequestParam(defaultValue = "") String oldName ){
        if(bindingResult.hasErrors()){
            return "teamUpdateForm";
        } else {
            teamService.UpdateTeam(oldName, team.getName(), team.getPoints());
            return "succesUpdate";
        }
    }

    @GetMapping("/addTeam")
    public String addTeam(Model model) {
        model.addAttribute("team", new Team());

        return "/teamAddForm";
    }

    @PostMapping("/addTeam")
    public String addTeamPost(@Valid Team team, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            return "teamAddForm";
        }
        else{
            teamService.CreateTeam(team);
            //return "registerForm";
            return "succes2";
        }
    }

    @GetMapping("/addTeamToLeague")
    public String addTeamToLeague(Model model)
    {
        return "teamToLeagueForm";
    }



    @PostMapping("/addTeamToLeague")
    public String addTeamToLeaguePost(Model model, @RequestParam(defaultValue = "") String team, @RequestParam(defaultValue = "") String league){

        List<Team> teams = teamService.findByName(team);
        List<League> leagues = leagueService.findByName(league);
        if(teams.size()!= 1){
            model.addAttribute("existsteam", true);
            return "operationsPanel";
        }
        else{
            if(leagues.size() !=  1){
                model.addAttribute("existsleague", true);
                return "operationsPanel";
            }
            else{
                leagueService.UpdateLeague(leagues.get(0), teams.get(0));
                teamService.UpdateTeamLeague(teams.get(0), leagues.get(0));
                System.out.println(leagues.get(0).getName());
                System.out.println(teams.get(0).getName());
            }
        }

        return "operationsPanel";
    }


    @GetMapping("/addPlayerToTeam")
    public String addPlayerToTeam(Model model)
    {
        return "playerToTeamForm";
    }

    @PostMapping("/addPlayerToTeam")
    public String addPlayerToTeamPost(Model model, @RequestParam(defaultValue = "") String firstname,
                                      @RequestParam(defaultValue = "") String lastname,
                                      @RequestParam(defaultValue = "") String position,
                                      @RequestParam(defaultValue = "") String team){

        List<Team> teams = teamService.findByName(team);
        List<Player> players = playerService.findByBothNamesExactly(firstname,lastname);

        System.out.println(teams.size());
        System.out.println(players.size());

        if(teams.size()!= 1){
            model.addAttribute("moreteams", true);
            return "operationsPanel";
        }
        else{
            int i=0;
            while(i<players.size()){
                if(!players.get(i).getPosition().equals(position)){
                    players.remove(i);
                }
                else{
                    i+=1;
                }
            }
            System.out.println(players.size());
            if(players.size() !=  1){
                model.addAttribute("moreplayers", true);
                return "operationsPanel";
            }
            else{
                teamService.UpdateSquad(teams.get(0),players.get(0));
                playerService.updatePlayerTeam(players.get(0), teams.get(0));
                System.out.println(players.get(0).getFirstname() + " " + players.get(0).getLastname());
                System.out.println(teams.get(0).getName());
            }
        }

        return "operationsPanel";
    }

    @GetMapping("/deletePlayer")
    public String deletePlayer(Model model)
    {
        return "playerDeleteForm";
    }

    @PostMapping("/deletePlayer")
    public String deletePLayerPost(Model model, @RequestParam(defaultValue = "") String firstname,
                                      @RequestParam(defaultValue = "") String lastname,
                                      @RequestParam(defaultValue = "") String position){

        List<Player> players = playerService.findByBothNamesExactly(firstname,lastname);

        System.out.println(players.size());

        int i=0;
        while(i<players.size()){
            if(!players.get(i).getPosition().equals(position)){
                players.remove(i);
                }
            else{
                i+=1;
                }
            }
        System.out.println(players.size());
        if(players.size() !=  1){
            model.addAttribute("moreplayers", true);
            return "operationsPanel";
        }
        else{
                playerService.deletePlayer(players.get(0));
                System.out.println(players.get(0).getFirstname() + " " + players.get(0).getLastname());
            }

        return "operationsPanel";
    }


    @GetMapping("/deleteTeam")
    public String deleteTeam(Model model)
    {
        return "teamDeleteForm";
    }

    @PostMapping("/deleteTeam")
    public String deleteTeamPost(Model model, @RequestParam(defaultValue = "") String name,
                                   @RequestParam(defaultValue = "") int points){

        List<Team> teams = teamService.findByName(name);

        System.out.println(teams.size());

        System.out.println(teams.size());
        if(teams.size() !=  1){
            model.addAttribute("moreteams", true);
            return "operationsPanel";
        }
        else{
            teamService.deleteTeam(teams.get(0));
            System.out.println(teams.get(0).getName() + " " + teams.get(0).getPoints());
        }

        return "operationsPanel";
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    public String uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("year") int year, @RequestParam("month") int month,  Model model) {

        model.addAttribute("month", month);
        model.addAttribute("year", year);
        log.info("User " + " is trying to upload a report for year: " + year + " and month: " + month);
        try {
            /**
             * Validation for the files names. If in DB there is already a file with that name in the same year, the user will have to rename his
             */
            Date today = new Date();
            boolean filenameExists = fileUploadService.verifyOnlyOneFileExistsWithNameAndYear(file.getOriginalFilename(), year);
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            Path path = Paths.get(UPLOAD_DIR + fileName);
            if(filenameExists){
                fileUploadService.updateFileWithInformation(file, String.valueOf(path), String.valueOf(today), year, month, fileName);
                Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
            } else{
                Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
                fileUploadService.saveUploadedFileInformation(file, String.valueOf(path), String.valueOf(today), year, month);

            }



            //boolean filenameExists = fileUploadService.verifyOnlyOneFileExistsWithNameAndYear(file.getOriginalFilename(), year);



            //Short validation in order for a single file to be present for any month-year combination
            /*FileUpload existingReport = fileUploadService.findFileByMonthAndYear(year, month);
            if (existingReport != null) {
                fileUploadService.deleteFile(existingReport);
            }

            //Moving the uploaded file into a Tomcat-Temp folder
            String realPath = request.getServletContext().getRealPath(UPLOAD_FILES_TEMPORARY_FOLDER);

            File temporaryDirectory = new File(realPath);
            if (!temporaryDirectory.exists()) {
                temporaryDirectory.mkdirs();
            }

            File uploadsDirectory = new File(FILEROOT_REPORTS_LOCATION + year + "/");
            if (!uploadsDirectory.exists()) {
                uploadsDirectory.mkdirs();
            }

            File transferFile = new File(realPath + "/" + file.getOriginalFilename());
            file.transferTo(transferFile);

            File sourceFile = transferFile;
            File destinationFile = new File(FILEROOT_REPORTS_LOCATION + year + "/" + file.getOriginalFilename());
            try {
                FileUtils.copyFile(sourceFile, destinationFile);
                log.info("File " + file.getOriginalFilename() + " has been successfully copied from source path " +
                        realPath + "to destination " + FILEROOT_REPORTS_LOCATION + year + "/");
                FileUtils.deleteQuietly(sourceFile);
                log.info("File " + file.getOriginalFilename() + " has been deleted from its temporary location " + realPath);

            } catch (IOException e) {
                log.error("Exception occurred while trying to copy the file from the temporary folder " + realPath +
                        " to the actual folder " + FILEROOT_REPORTS_LOCATION + year + "/");
                return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.BAD_REQUEST, OfficeTimeMessages.MSG_FILE_FROM_TEMP_TO_ACTUAL_DIRECTORY, ApiSeverity.DANGER);
            }

            Date date = new Date();
            reportService.saveUploadedFileInformation(file, destinationFile.getPath(), sessionUsername, date, year, month);

        } catch (Exception e) {
            log.error("File could not be uploaded");
            return ApiResponseFactory.buildErrorResponseMessage(HttpStatus.BAD_REQUEST, OfficeTimeMessages.MSG_FILE_UPLOAD_FAILED, ApiSeverity.DANGER);
        }
        return ApiResponseFactory.buildSuccessResponseMessage(OfficeTimeMessages.MSG_FILE_UPLOAD_SUCCESS);*/
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "fileUploadSucces";
    }

    @PostMapping("/remove")
    public String deleteFile(@ModelAttribute("filename") String filename){
        FileUpload fileUpload = fileUploadService.findByFilename(filename);
        fileUploadService.deleteFile(fileUpload);

        return "operationsPanel";
    }

    @GetMapping("/listFiles")
    public String listOfFiles(Model model){
        model.addAttribute("listOfFiles", fileUploadService.findAll());

        return "listOfFiles";

    }

    @PostMapping("/readFileAndMap")
    public String readFileAndMap(@ModelAttribute("filename") String filename){
        fileUploadService.readFileAndMapValues(filename);
        return "operationsPanel";
    }

    @PostMapping("/initLeagueSeason")
    public String initLeagueSeason(@RequestParam int year) {
        Boolean allSeasonEnded = tournamentService.verifySeasonEndAcrossGlobe();
        if(!allSeasonEnded){
            log.error("Not all seasons are finished");
            return "failedToInitializeNewSeason";
        }
        List<Tournament> tournamentList = tournamentService.findLastTournaments();
        for(Tournament tournament : tournamentList){
            if(tournament.getYear() != year - 1){
                log.error("The year is not the good one: " + year + "  was: " + tournament.getYear());
                return "failedToInitializeNewSeason";
            }

        }
        leagueService.initLeaguePerSeason(year);
        return "operationsPanel";
    }



    @GetMapping("/generateRoundsAcrossLeagues")
    public String generateRound(Model model){
        List<Round> roundList = tournamentService.initializeRound();
        for (Round round : roundList) {
            Tournament tournament = tournamentService.findById(round.getTournament().getId());
            if (tournament != null) {
                League league = leagueService.findById(tournament.getLeague().getId());
                if (league != null) {
                    log.info("For " + league.getName() + " the round " + round.getRoundNumber() + " was generated");
                    footballUtils.GenerateMatchPerRound(league.getTeamList(), tournament, round);
                }
            }
        }

        List<Tournament> tournamentList = tournamentService.verifySeasonEnd();
        if (tournamentList.size() != 0) {
            for (Tournament t : tournamentList) {
                log.info("Tournament " + t.getLeague().getName() + " has ended");
            }
        }

        Boolean checkEndingLeagues = tournamentService.verifySeasonEndAcrossGlobe();

        List<Round> roundListMatchday = new ArrayList<>();
        List<League> activeLeagues = leagueService.findAll();
        for(League league : activeLeagues) {
            if (league.getTournamentList().size() != 0) {
                List<Tournament> tournamentListMatchday = league.getTournamentList();
                tournamentListMatchday.sort(Comparator.comparing(Tournament::getYear).reversed());
                Tournament tournament = tournamentListMatchday.get(0);
                Round round = roundService.findByRoundNumberAndYear(tournament.getLastRoundPlayed(), tournament.getYear(), tournament.getId());
                if(round != null){
                    roundListMatchday.add(round);
                }
            }
        }
        model.addAttribute(roundListMatchday);
        return "matchday";
    }
}