package com.football.Cron;

import com.football.domain.League;
import com.football.domain.Round;
import com.football.domain.Tournament;
import com.football.service.LeagueService;
import com.football.service.TournamentService;
import com.football.serviceUtils.FootballUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Component
@Transactional
public class GenerateRoundCronjob {

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private LeagueService leagueService;

    @Autowired
    private FootballUtils footballUtils;

    //@Scheduled(cron = "0 42 1 * * ?")
    @Transactional
    public String CronGenerateRound(){
        List<Round> roundList = tournamentService.initializeRound();
        for (Round round : roundList) {
            Tournament tournament = tournamentService.findById(round.getTournament().getId());
            if (tournament != null) {
                League league = leagueService.findById(tournament.getLeague().getId());
                if (league != null) {
                    log.info("For " + league.getName() + " the round " + round.getRoundNumber() + " was generated");
                    footballUtils.GenerateMatchPerRound(league.getTeamList(), tournament, round);
                }
            }
        }

        List<Tournament> tournamentList = tournamentService.verifySeasonEnd();
        if (tournamentList.size() != 0) {
            for (Tournament t : tournamentList) {
                log.info("Tournament " + t.getLeague().getName() + " has ended");
            }
        }

        Boolean checkEndingLeagues = tournamentService.verifySeasonEndAcrossGlobe();

        return "succes";
    }

}
